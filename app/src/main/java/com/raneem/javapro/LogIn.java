package com.raneem.javapro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class LogIn extends AppCompatActivity {
    TextView notuser;
    EditText email,password;;
    Button login;
    TextView forgotPassword_textButton;
    FirebaseAuth mFirebaseAuth;
    DatabaseReference mDatabase;
    StorageReference storageRef;
    FirebaseDatabase firebaseDatabase;
    slide ss;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        mFirebaseAuth = FirebaseAuth.getInstance();

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        login = (Button) findViewById(R.id.button3);
        forgotPassword_textButton = (TextView) findViewById(R.id.texetButton);
        notuser = (TextView) findViewById(R.id.textView6);
        storageRef= FirebaseStorage.getInstance().getReference();
        firebaseDatabase = FirebaseDatabase.getInstance();




    }

    public void RegisterPage(View view){


        Intent register=new Intent(getApplicationContext(),MainActivity.class);
        startActivity(register);
    }

    public void Login(View view){
    String uEmail=email.getText().toString();
    String uPassword=password.getText().toString();
    mFirebaseAuth.signInWithEmailAndPassword(uEmail, uPassword)
    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
    if (task.isSuccessful()) {

        firebaseDatabase.getReference("javatwoslide").removeValue();
        //slide s=new slide("title", "fipath");
        //slide s1=new slide("Introduction", "https://firebasestorage.googleapis.com/v0/b/javapro-6e07e.appspot.com/o/upload%2F%20Introduction.pdf?alt=media&token=2f5ed82b-a2db-4c64-9c7d-b155792d0112");
        //slide s2=new slide("JavaBasics(I)", "https://firebasestorage.googleapis.com/v0/b/javapro-6e07e.appspot.com/o/upload%2FJavaBasics%20(I).pdf?alt=media&token=95d8e19d-14cb-408d-b271-940e6c87c306");

        //firebaseDatabase.getReference("javaoneslide").push().setValue(s1);
        //firebaseDatabase.getReference("javaoneslide").push().setValue(s2);

        slide s1 = new slide("Polymorphism","https://firebasestorage.googleapis.com/v0/b/javapro-6e07e.appspot.com/o/upload%2FLec7%20-%20Polymorphism.pdf?alt=media&token=ab52139d-97cd-432b-b79c-a95115acec8e");
        slide s2 = new slide("Interfaces","https://firebasestorage.googleapis.com/v0/b/javapro-6e07e.appspot.com/o/upload%2FLec8-%20Interfaces(1).pdf?alt=media&token=7b8afe0d-8f6c-43e5-8f5e-992f1cdca02c");
        slide s3 = new slide("Exception Handling","https://firebasestorage.googleapis.com/v0/b/javapro-6e07e.appspot.com/o/upload%2FLec9-%20Exception%20Handling.pdf?alt=media&token=75e03da2-b78d-4d60-baea-e104110e108f");
        slide s4 = new slide("Introduction To Streams","https://firebasestorage.googleapis.com/v0/b/javapro-6e07e.appspot.com/o/upload%2FLec10%20-%20Introduction%20to%20Streams.pdf?alt=media&token=d4462114-cdd9-4171-9fc8-a12979a009b0");

        firebaseDatabase.getReference("javatwoslide").push().setValue(s1);
        firebaseDatabase.getReference("javatwoslide").push().setValue(s2);
        firebaseDatabase.getReference("javatwoslide").push().setValue(s3);
        firebaseDatabase.getReference("javatwoslide").push().setValue(s4);

    // Sign in success, update UI with the signed-in user's information
    Log.d("my_stor", "createUserWithEmail:success");
     FirebaseUser user = mFirebaseAuth.getCurrentUser();
        Intent intent = new Intent(getApplicationContext(), listofslides.class);
        startActivity(intent);
      Toast.makeText(getApplicationContext(),"You've logged in successfully",Toast.LENGTH_SHORT).show();
    }
    else {
      // If sign in fails, display a message to the user.
     Log.w("my_stor", "createUserWithEmail:failure", task.getException());
       Toast.makeText(getApplicationContext(),"login failed.\nMake sure you entered valid email and password",Toast.LENGTH_SHORT).show();
  }
 }
    });


    }


public void Forgot_Password(View view){

    Intent forgot_pass=new Intent(getApplicationContext(),ForgotPassword.class);
    startActivity(forgot_pass);

}


}


